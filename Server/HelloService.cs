﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;
using System;

namespace Server
{
    internal class HelloService : Generated.HelloService.HelloServiceBase
    {
        public override Task<EmptyResponse> SayHello(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine(request.Name);
            return Task.FromResult(new EmptyResponse());
        }
    }
}
