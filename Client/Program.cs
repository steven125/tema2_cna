﻿using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Enter your name: ");
            string name = Console.ReadLine();
            var client = new Generated.HelloService.HelloServiceClient(channel);
            client.SayHello(new Generated.HelloRequest() {Name = name });
            

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
